db.posts.ensureIndex({date:-1})

db.posts.find().sort({date: -1}).explain()

db.posts.ensureIndex({permalink:1})

db.posts.ensureIndex({tags:1, date:-1})

db.posts.find({tags:"dinosaur"}).sort({date:-1}).explain()
