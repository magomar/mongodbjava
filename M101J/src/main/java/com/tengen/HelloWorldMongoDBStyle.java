package com.tengen;

import com.mongodb.*;

import java.net.UnknownHostException;

/**
 * Created by mario on 2/06/14.
 */
public class HelloWorldMongoDBStyle {
    public static void main( String[] args ) throws UnknownHostException {
        MongoClient client = new MongoClient();
        DB database = client.getDB("course");
        DBCollection collection = database.getCollection("hello");
        DBObject document = collection.findOne();
        System.out.println(document);
    }
}
