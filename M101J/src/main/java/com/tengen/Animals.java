package com.tengen;

import com.mongodb.*;

import java.net.UnknownHostException;

/**
 * Created by mario on 15/07/14.
 */
public class Animals {

    public static void main(String[] args) throws UnknownHostException {
        MongoClient c =  new MongoClient(new MongoClientURI("mongodb://localhost"));
        DB db = c.getDB("test");
        DBCollection animals = db.getCollection("animals");

        BasicDBObject animal = new BasicDBObject("animal", "monkey");

        animals.insert(animal);
        animal.removeField("animal");
        animal.append("animal", "cat");
        animals.insert(animal);
        animal.removeField("animal");
        animal.append("animal", "lion");
        animals.insert(animal);
    }
}
