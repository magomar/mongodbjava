import com.mongodb.*;
import org.bson.BSONObject;
import org.bson.BasicBSONObject;

import java.net.UnknownHostException;

/**
 * Created by mario on 12/06/14.
 * MongoDB for Java Developers
 * Homework 3.2
 * remove the grade of type "homework" with the lowest score for each student from the dataset
 */
public class PrepareDatabase {
    public static void main(String[] args) throws UnknownHostException {
        MongoClient client = new MongoClient();
        DB database = client.getDB("school");
        DBCollection collection = database.getCollection("students");
//        Write a program in the language of your choice that will remove the lowest homework score for each student. Since there is a single
//        document for each student containing an array of scores, you will need to update the scores array and remove the homework.
//        Hint/spoiler: With the new schema, this problem is a lot harder and that is sort of the point. One way is to find the lowest homework
//        in code and then update the scores array with the low homework pruned. If you are struggling with the Node.js side of this, look at
//        the .splice() operator, which can remove an element from an array in-place.
//        DBObject query = new BasicDBObject().append("type", "homework");
        DBCursor cursor = collection.find();
//        cursor.sort(new BasicDBObject("student_id", 1).append("score", 1));
//        int lastStudentId = -1;
        for (DBObject cur : cursor) {
            int studentId = ((BasicBSONObject) cur).getInt("_id");
//            String name = ((BasicBSONObject) cur).getString("name");
            BasicDBList scores = (BasicDBList) cur.get("scores");
            double lowestScore = 9999;
            int lowestScoreIndex = 10;
            for (int i = 0; i < scores.size(); i++) {
                BasicBSONObject scoreObject = (BasicBSONObject) scores.get(i);
                String scoreType = scoreObject.getString("type");
                double scoreValue = scoreObject.getDouble("score");
                if ("homework".equals(scoreType) && scoreValue < lowestScore) {
                    lowestScore = scoreValue;
                    lowestScoreIndex = i;
                }
            }

            scores.remove(lowestScoreIndex);
//            System.out.println(scores);
            BasicDBObject match = new BasicDBObject("_id", studentId);
            BasicDBObject update = new BasicDBObject("scores", scores);
            collection.update(match, new BasicDBObject("$set", update));
        }
    }
}
