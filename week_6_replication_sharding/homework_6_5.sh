# clean everything up
echo "killing mongod and mongos"
killall mongod
killall mongos
echo "removing data files"
rm -rf /data/rs*

# Create data directories
echo "creating fresh new data files"
mkdir -p /data/rs1 /data/rs2 /data/rs3

# start replica set mongod processes
echo "starting replica set processes"
mongod --replSet m101 --logpath "1.log" --dbpath /data/rs1 --port 27017 --smallfiles --oplogSize 64 --fork
mongod --replSet m101 --logpath "2.log" --dbpath /data/rs2 --port 27018 --smallfiles --oplogSize 64 --fork
mongod --replSet m101 --logpath "3.log" --dbpath /data/rs3 --port 27019 --smallfiles --oplogSize 64 --fork

# connect to one mongod and initiate the set
echo "Waiting 5 seconds "
sleep 5
echo "Connnecting to one server and configuring replica set"
mongo --port 27017 << 'EOF'
config = { _id: "m101", members:[
          { _id : 0, host : "localhost:27017"},
          { _id : 1, host : "localhost:27018"},
          { _id : 2, host : "localhost:27019"} ]
};
rs.initiate(config)
EOF

#Check status
echo "Waiting 60 seconds for the replica set to fully come online"
sleep 60
echo "Checking status"
mongo --port 27017 << 'EOF'
rs.status()
EOF

