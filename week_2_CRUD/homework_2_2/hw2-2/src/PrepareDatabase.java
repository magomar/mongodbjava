import com.mongodb.*;
import org.bson.BSONObject;
import org.bson.BasicBSONObject;

import java.net.UnknownHostException;

/**
 * Created by mario on 10/06/14.
 * MongoDB for Java Developers
 * Homework 2.2
 * remove the grade of type "homework" with the lowest score for each student from the dataset
 */
public class PrepareDatabase {
    public static void main(String[] args) throws UnknownHostException {
        MongoClient client = new MongoClient();
        DB database = client.getDB("students");
        DBCollection collection = database.getCollection("grades");
        //If you select homework grade-documents, sort by student and then by score,
        //you can iterate through and find the lowest score for each student by noticing a change in student id. As you
        // notice that change of student_id, remove the document.
        DBObject query = new BasicDBObject().append("type", "homework");
        DBCursor cursor = collection.find(query);
        cursor.sort(new BasicDBObject("student_id", 1).append("score", 1));
        int lastStudentId = -1;
        for (DBObject object : cursor) {
            int studentId = ((BasicBSONObject) object).getInt("student_id");
            if (studentId != lastStudentId) {
                lastStudentId = studentId;
                collection.remove(object);
                System.out.println("Removed " + object.toString());
            }
        }

    }
}
